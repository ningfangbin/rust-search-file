pub mod config {
    pub struct FileConfig {
        pub query: String,
        pub file_patch: String,
    }

    impl FileConfig {
        pub fn build(args: &[String]) -> Result<FileConfig, &str> {
            if args.len() < 3 {
                return Err("not enough arguments");
            }
            let query = args[1].clone();
            let file_patch = args[2].clone();
            Ok(FileConfig { query, file_patch })
        }
    }
}
pub mod find {
    use super::config::FileConfig;
    use std::{error::Error, fs};
    pub fn run(config: FileConfig) -> Result<(), Box<dyn Error>> {
        let buf = fs::read_to_string(config.file_patch)?;
        for (num, line) in search(&config.query, &buf).into_iter().enumerate() {
            println!("{num}:{line}");
        }
        Ok(())
    }
    pub fn search<'a>(query: &str, contents: &'a str) -> Vec<&'a str> {
        let mut result = Vec::new();
        for line in contents.lines() {
            if line.contains(query) {
                result.push(line);
            }
        }
        result
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn one_result() {
        let query = "duct";
        let contents = "\
Rust:
safe, fast, productive.
Pick three.";

        assert_eq!(
            vec!["safe, fast, productive."],
            find::search(query, contents)
        );
    }
}
