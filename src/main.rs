use std::{env,  process};
mod find;
use find::config::FileConfig;
fn main() {
    let args: Vec<String> = env::args().collect();
    let file_info = FileConfig::build(&args).unwrap_or_else(|err| {
        println!("Build file config failed: {}", err);
        process::exit(-1);
    });
    // println!("find info:{:?}in file<{:?}>", query, file_patch);
     find::find::run(file_info).unwrap_or_else(|err|{
        println!("Run failed: {}", err);
        process::exit(-1);
    });

    // if let Err(err) = run(file_info) {
    //     println!("Application error: {err}",);
    //     process::exit(1);
    // }
}

